/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katas.fizzbuzz.rules;

import com.katas.fizzbuzz.interfaces.IRule;

/**
 *
 * @author cyberlight
 */
public class DefaultRule implements IRule{

    private int acceptedValue;

    public DefaultRule() {
        acceptedValue = -1;
    }
    
    @Override 
    public boolean IsAccepted(int i) {
        acceptedValue = i;
        return true;
    }

    @Override
    public String getValue() {
        return String.valueOf(acceptedValue);
    }
    
}
