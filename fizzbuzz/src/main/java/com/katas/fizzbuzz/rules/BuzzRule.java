/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katas.fizzbuzz.rules;

/**
 * @author cyberlight
 */
public class BuzzRule extends BaseRule {
  public BuzzRule() {
    super(5, "Buzz");
  }
}
