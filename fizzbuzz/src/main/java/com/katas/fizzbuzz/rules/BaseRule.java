package com.katas.fizzbuzz.rules;

import com.katas.fizzbuzz.interfaces.IRule;

public class BaseRule implements IRule {
  private final int multiples;
  private final String text;

  public BaseRule(int multiples, String text) {
    this.multiples = multiples;
    this.text = text;
  }

  @Override
  public boolean IsAccepted(int i) {
    return i % multiples == 0;
  }

  @Override
  public String getValue() {
    return text;
  }
}