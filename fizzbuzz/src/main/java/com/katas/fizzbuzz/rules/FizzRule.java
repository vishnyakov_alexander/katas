/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katas.fizzbuzz.rules;

/**
 * @author cyberlight
 */
public class FizzRule extends BaseRule {
  public FizzRule() {
    super(3, "Fizz");
  }
}
