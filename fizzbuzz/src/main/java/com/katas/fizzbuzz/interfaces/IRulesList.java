/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katas.fizzbuzz.interfaces;

import com.katas.fizzbuzz.interfaces.IRule;

/**
 *
 * @author cyberlight
 */
public interface IRulesList {

    boolean IsExists(IRule rule);

    void addRule(IRule rule) throws Exception;

    IRule getRule(int i);

    int size();
    
}
