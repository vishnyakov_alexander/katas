/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katas.fizzbuzz.interfaces;

/**
 *
 * @author cyberlight
 */
public interface IRule {

    boolean IsAccepted(int i);

    String getValue();
    
}
