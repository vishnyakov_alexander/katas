/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katas.fizzbuzz;

import com.katas.fizzbuzz.comparators.ByClassNameComparator;
import com.katas.fizzbuzz.interfaces.IRule;
import com.katas.fizzbuzz.interfaces.IRulesList;
import com.katas.fizzbuzz.lists.RulesList;


/**
 *
 * @author cyberlight
 */
public class BaseProcessor {

    private IRulesList rulesList;
    
    public BaseProcessor() {
        this.rulesList = new RulesList(new ByClassNameComparator());
    }

    public void addRule(IRule rule) throws Exception {
        rulesList.addRule(rule);
    }
    
    public IRule getRule(int index) {
        return rulesList.getRule(index);
    }
    
    public String process(int value) {
        IRule acceptedRule = null;
        for (int i = rulesList.size()-1; i >= 0; i--) {
            IRule rule = rulesList.getRule(i);
            if(rule.IsAccepted(value)){
                acceptedRule = rule;
                break;
            }
        }
        return acceptedRule.getValue();
    }
}
