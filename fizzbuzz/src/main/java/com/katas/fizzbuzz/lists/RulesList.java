/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katas.fizzbuzz.lists;

import com.katas.fizzbuzz.interfaces.IRule;
import com.katas.fizzbuzz.interfaces.IRulesList;
import com.katas.fizzbuzz.rules.DefaultRule;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author cyberlight
 */
public class RulesList implements IRulesList {

    private final List<IRule> rules;
    private final Comparator<IRule> comparator;
    
    public RulesList(Comparator<IRule> comparator) {
        rules = new ArrayList<IRule>();
        rules.add(new DefaultRule());
        this.comparator = comparator;
    }
    
    @Override
    public IRule getRule(int i) {
        return rules.get(i);
    }

    @Override
    public void addRule(IRule rule) throws Exception {
        if(IsExists(rule))
            throw new Exception(String.format("Rule of type %s already exists!", rule.getClass().getName()));
            
        rules.add(rule);
    }

    @Override
    public int size() {
        return rules.size();
    }

    @Override
    public boolean IsExists(IRule rule) {
        for (Iterator<IRule> it = rules.iterator(); it.hasNext();) {
            IRule ruleIt = it.next();
            if(comparator.compare(rule, ruleIt) == 0)
                return true;
        }
        return false;
    }
    
}
