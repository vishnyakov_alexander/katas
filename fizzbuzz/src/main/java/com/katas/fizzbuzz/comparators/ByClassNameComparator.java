/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katas.fizzbuzz.comparators;

import com.katas.fizzbuzz.interfaces.IRule;
import java.util.Comparator;

/**
 *
 * @author cyberlight
 */
public class ByClassNameComparator implements Comparator<IRule>{

    public int compare(IRule o1, IRule o2) {
        return o1.getClass().toString().compareTo(o2.getClass().toString());
    }
    
}
