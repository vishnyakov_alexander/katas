/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katas.fizzbuzz;

import com.katas.fizzbuzz.rules.BuzzRule;
import com.katas.fizzbuzz.rules.FizzBuzzRule;
import com.katas.fizzbuzz.rules.FizzRule;

/**
 * @author cyberlight
 */
public class FizzBuzzProcessor extends BaseProcessor {
    
    public FizzBuzzProcessor() throws Exception {
        addRule(new FizzRule());
        addRule(new BuzzRule());
        addRule(new FizzBuzzRule());
    }
}
