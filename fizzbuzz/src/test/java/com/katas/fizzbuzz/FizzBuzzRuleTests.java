/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katas.fizzbuzz;

import com.katas.fizzbuzz.rules.FizzBuzzRule;
import org.junit.*;

/**
 *
 * @author cyberlight
 */
public class FizzBuzzRuleTests {
    
    private FizzBuzzRule fizzBuzzRule;
    
    public FizzBuzzRuleTests() {
        fizzBuzzRule = new FizzBuzzRule();
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testFizzBuzz_TrySetANumberWhichIsDividedIntoThreeAndFive_CheckWasIsAcceptedReturnTrue(){
        Assert.assertTrue(fizzBuzzRule.IsAccepted(15));
    }
    
    @Test
    public void testFizzBuzz_TrySetANumberWhichIsNotDividedIntoThreeAndFive_CheckWasIsAcceptedReturnFalse(){
        Assert.assertFalse(fizzBuzzRule.IsAccepted(1));
    }
    
    @Test
    public void testFizzBuzz_TryGetValueOfRule_CheckWasValueIsFizzBuzz(){        
        Assert.assertEquals("FizzBuzz", fizzBuzzRule.getValue());
    }
}