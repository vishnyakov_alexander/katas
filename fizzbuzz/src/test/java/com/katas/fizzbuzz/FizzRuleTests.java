/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katas.fizzbuzz;

import com.katas.fizzbuzz.rules.FizzRule;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author cyberlight
 */
public class FizzRuleTests {
    
    private FizzRule fizzRule;
    
    public FizzRuleTests() {
        fizzRule = new FizzRule();
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testFizz_TrySetANumberWhichIsDividedIntoThree_CheckWasIsAcceptedReturnTrue(){
        Assert.assertTrue(fizzRule.IsAccepted(3));
    }
    
    @Test
    public void testFizz_TrySetANumberWhichIsNotDividedIntoThree_CheckWasIsAcceptedReturnFalse(){
        Assert.assertFalse(fizzRule.IsAccepted(1));
    }
    
    @Test
    public void testFizz_TryGetValueOfRule_CheckWasValueIsFizz(){        
        Assert.assertEquals("Fizz", fizzRule.getValue());
    }
}
