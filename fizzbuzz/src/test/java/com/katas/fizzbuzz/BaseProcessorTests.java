/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katas.fizzbuzz;

import com.katas.fizzbuzz.rules.DefaultRule;
import com.katas.fizzbuzz.rules.FizzRule;
import org.junit.*;

/**
 *
 * @author cyberlight
 */
public class BaseProcessorTests {
    
    private BaseProcessor baseProcessor;
    
    public BaseProcessorTests() {
        baseProcessor = new BaseProcessor();
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testBaseProcessor_TryGetRulesOfProcessor_CheckWasFirstRuleAreDefaultRule(){
        Assert.assertTrue(baseProcessor.getRule(0).getClass() == DefaultRule.class);
    }

    @Test
    public void testBaseProcessor_TryAddRule_CheckWasRuleAddedAtTheEndOfRules() throws Exception{
        baseProcessor.addRule(new FizzRule());
        Assert.assertTrue(baseProcessor.getRule(1).getClass() == FizzRule.class);
    }

    @Test(expected = Exception.class)
    public void testBaseProcessor_TryAddDuplicateRule_CheckWasExceptionThrown() throws Exception{
        baseProcessor.addRule(new DefaultRule());
        Assert.fail("Exception was not thown, when duplicate rule added!!!");
    }
}
