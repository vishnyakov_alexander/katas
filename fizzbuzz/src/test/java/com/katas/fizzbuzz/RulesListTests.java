/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katas.fizzbuzz;

import com.katas.fizzbuzz.comparators.ByClassNameComparator;
import com.katas.fizzbuzz.interfaces.IRule;
import com.katas.fizzbuzz.lists.RulesList;
import com.katas.fizzbuzz.rules.BuzzRule;
import com.katas.fizzbuzz.rules.DefaultRule;
import com.katas.fizzbuzz.rules.FizzBuzzRule;
import com.katas.fizzbuzz.rules.FizzRule;
import org.junit.*;

/**
 *
 * @author cyberlight
 */
public class RulesListTests {
    
    private RulesList rulesList;
    
    public RulesListTests() {
        rulesList = new RulesList(new ByClassNameComparator());
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testList_TryGetRules_CheckWasFirstRuleIsDefaultRule(){
        IRule rule = rulesList.getRule(0);
        Assert.assertEquals(DefaultRule.class, rule.getClass()); 
    }
    
    @Test
    public void testList_TryAddRule_CheckWasRuleAddedAtTheEndOfList() throws Exception{
        rulesList.addRule(new FizzRule());
        IRule rule = rulesList.getRule(rulesList.size()-1);
        Assert.assertEquals(FizzRule.class, rule.getClass()); 
    }
    
    @Test(expected = Exception.class)
    public void testList_TryAddDuplicateByClassNameRule_CheckWasRaisedException() throws Exception{
        rulesList.addRule(new FizzRule());
        rulesList.addRule(new FizzRule());
        Assert.fail("Exception not thrown, when duplicate rule added!");
    }
    
    @Test
    public void testList_TryAddSomeRules_CheckSizeOfList() throws Exception{
        rulesList.addRule(new FizzRule());
        rulesList.addRule(new BuzzRule());
        rulesList.addRule(new FizzBuzzRule());
        Assert.assertEquals(4, rulesList.size()); 
    }
}
