/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katas.fizzbuzz;

import org.junit.*;

/**
 *
 * @author cyberlight
 */
public class FizzBuzzProcessorTests {
    
    private FizzBuzzProcessor fizzBuzzProcessor;
    private static final String FIZZ = "Fizz";
    private static final String BUZZ = "Buzz";
    private static final String FIZZ_BUZZ = "FizzBuzz";
    
    public FizzBuzzProcessorTests() throws Exception {
        fizzBuzzProcessor = new FizzBuzzProcessor();
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testFizz_TryPassANumberWhichIsDividedIntoThree_CheckWasReturnedFizz(){
        String result = fizzBuzzProcessor.process(3);
        Assert.assertEquals(FIZZ, result);
    }
    
    @Test
    public void testBuzz_TryPassANumberWhichIsDividedIntoFive_CheckWasReturnedBuzz(){
        String result = fizzBuzzProcessor.process(5);
        Assert.assertEquals(BUZZ, result);
    }
    
    @Test
    public void testNotFizzBuzzNumbers_TryPassANumberWhichIsNotDividedIntoFiveOrThree_CheckWasReturnedTextEquivalentOfNumber(){
        String result = fizzBuzzProcessor.process(1);
        Assert.assertEquals("1", result);
    }
    
    @Test
    public void testFizzBuzzAndNumbers_TryPassARangeOfNumbers_CheckWasReturnedValuesAreValid(){
        int[] testValues = new int[] {1,2,3,4,5,6,7};
        String[] testResults = new String[] {"1","2", FIZZ, "4", BUZZ, FIZZ, "7"};
        
        for(int i = 0; i< testValues.length; i++){
            Assert.assertEquals(testResults[i], fizzBuzzProcessor.process(testValues[i]));
        }
    }
    
    @Test
    public void testFizzBuzzNumber_TryPassANumberWhichIsDividedIntoFiveAndThree_CheckWasReturnedFizzBuzz(){
        String result = fizzBuzzProcessor.process(15);
        Assert.assertEquals(FIZZ_BUZZ, result);
    }
    
    @Test
    public void testFizzBuzzAndNumbers_TryPassARangeOfNumbersFrom1To15_CheckWasReturnedValuesAreValid(){
        int[] testValues = new int[] {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
        String[] testResults = new String[] {"1","2", FIZZ, "4", BUZZ, FIZZ, "7", "8", 
                                             FIZZ, BUZZ, "11", FIZZ, "13", "14", FIZZ_BUZZ};
        
        for(int i = 0; i< testValues.length; i++){
            Assert.assertEquals(testResults[i], fizzBuzzProcessor.process(testValues[i]));
        }
    }
    
}
