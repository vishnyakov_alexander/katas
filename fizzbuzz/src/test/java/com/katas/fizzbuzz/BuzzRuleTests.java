/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katas.fizzbuzz;

import com.katas.fizzbuzz.rules.BuzzRule;
import org.junit.*;

/**
 *
 * @author cyberlight
 */
public class BuzzRuleTests {
    
    private BuzzRule buzzRule;
    
    public BuzzRuleTests() {
        buzzRule = new BuzzRule();
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testBuzz_TrySetANumberWhichIsDividedIntoFive_CheckWasIsAcceptedReturnTrue(){
        Assert.assertTrue(buzzRule.IsAccepted(5));
    }
    
    @Test
    public void testBuzz_TrySetANumberWhichIsNotDividedIntoFive_CheckWasIsAcceptedReturnFalse(){
        Assert.assertFalse(buzzRule.IsAccepted(1));
    }
    
    @Test
    public void testBuzz_TryGetValueOfRule_CheckWasValueIsBuzz(){
        Assert.assertEquals("Buzz", buzzRule.getValue());
    }
}
