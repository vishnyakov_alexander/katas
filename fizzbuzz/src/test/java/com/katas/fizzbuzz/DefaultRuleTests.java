/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katas.fizzbuzz;

import com.katas.fizzbuzz.rules.DefaultRule;
import org.junit.*;

/**
 *
 * @author cyberlight
 */
public class DefaultRuleTests {
    private static final int TEST_VALUE_21 = 21;
    
    private DefaultRule defaultRule;
    
    public DefaultRuleTests() {
        defaultRule = new DefaultRule();
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testDefaultRule_TryCheckAcceptanceOfSomethingValue_CheckWasValueIsAccepted(){
        Assert.assertTrue(defaultRule.IsAccepted(TEST_VALUE_21));
    }
    
    @Test
    public void testDefaultRule_TryCheckAcceptanceOfSomethingValue_CheckWasGetValueReturnAcceptedValueAsString(){
        defaultRule.IsAccepted(TEST_VALUE_21);
        Assert.assertEquals(String.valueOf(TEST_VALUE_21), defaultRule.getValue());
    }
}
